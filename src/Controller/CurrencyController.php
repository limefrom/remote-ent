<?php

namespace App\Controller;

use App\Entity\Currency;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;

/**
 * @Rest\RouteResource("Currency")
 */
class CurrencyController implements ClassResourceInterface
{
    /**
     * Get a Currency entity
     */
    public function getAction(Currency $currency)
    {
        return $currency;
    }
}