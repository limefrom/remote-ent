<?php


namespace App\Service;


use App\Entity\Currency;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Security\Core\Exception\LockedException;

/**
 * Инфраструктурный класс
 */
class CurrencyRemoteProvider implements CurrencyProviderInterface
{
    const LOCK_TTL = 10000000;

    /**
     * @var CacheInterface $cache
     */
    private $cache;

    /**
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $id
     * @return Currency|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function get(string $id): ?Currency
    {
        $lockName = $this->getLockName($id);

        if ($this->cache->has($lockName)) {
            throw new LockedException();
        }

        $currency = null;

        $this->cache->set($lockName, 1, self::LOCK_TTL);

        try {
            if ($id === 'EUR') {
                $currency = new Currency('EUR', 'Euro', 75.784);// remote request stub
            }
        } finally {
            $this->cache->delete($lockName);
        }

        return $currency;
    }

    /**
     * @param string $id
     * @return string
     */
    private function getLockName(string $id): string
    {
        return 'currency.lock.' . $id;
    }

}