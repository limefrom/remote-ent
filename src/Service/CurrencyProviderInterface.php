<?php

namespace App\Service;


use App\Entity\Currency;

interface CurrencyProviderInterface
{
    public function get(string $id): ?Currency;
}