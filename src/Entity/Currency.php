<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class Currency
{
    /**
     * @var int
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     */
    protected $code;

    /** @var string */
    protected $description;

    /** @var float */
    protected $value;

    /**
     * @var \DateTime
     * @JMS\Exclude();
     */
    protected $updated_at;

    /**
     * Currency constructor.
     * Обязательные поля в конструктор.
     *
     * @param string $code
     * @param string $description
     * @param float $value
     */
    public function __construct(string $code, string $description, float $value)
    {
        $this->code = $code;
        $this->description = $description;
        $this->value = $value;
        $this->updated_at = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function updateValue(float $value)
    {
        $this->value = $value;
        $this->updated_at = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at;
    }
}