<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Service\CurrencyProviderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Security\Core\Exception\LockedException;

class CurrencyRepository extends ServiceEntityRepository
{
    /** seconds */
    const CACHE_TTL = 600;

    /** seconds */
    const DB_TTL = 3600;

    const MAX_GET_REMOTE_ATTEMPTS = 3;

    /** microseconds */
    const REMOTE_ATTEMPTS_DELAY = 100000;

    /**
     * @var CacheInterface $cache
     */
    private $cache;

    /**
     * @var CurrencyProviderInterface $remoteProvider
     */
    private $remoteProvider;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param CacheInterface $cache
     * @param CurrencyProviderInterface $remoteProvider
     */
    public function __construct(ManagerRegistry $managerRegistry, CacheInterface $cache, CurrencyProviderInterface $remoteProvider)
    {
        $this->cache = $cache;
        $this->remoteProvider = $remoteProvider;

        parent::__construct($managerRegistry, Currency::class);
    }

    /**
     * @inheritdoc
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $currency = $this->findInCacheOrDB($id);

        if (!$currency || $currency->getUpdatedAt() < new \DateTime('- ' . self::DB_TTL . ' seconds')) {
            $currency = $this->getFromRemoteProvider($id);
        }

        return $currency;
    }

    /**
     * @param string $id
     * @return Currency|mixed|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function findInCacheOrDB(string $id): ?Currency
    {
        $cacheName = $this->getCacheName($id);

        if ($this->cache->has($cacheName)) {
            $currency = $this->cache->get($cacheName);
        } else {
            $currency = $this->findInDB($id);
        }

        return $currency;
    }

    /**
     * @param $id
     * @return Currency|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function findInDB(string $id): ?Currency
    {
        $currency = null;

        if ($id === 'AED') {
            /*
             * заглушка $this->_em->find
             * валидацию я опустил, обычно я делаю класс создатель DTO+ и переношу ее туда
             * некоторые другие вещи, нужные только при создании, переносятся туда так же, что делает модель тоньше
            */
            $currency = new Currency('AED', 'United Arab Emirates dirham', 0.784);
        }

        if ($currency) {
            $this->cache->set($this->getCacheName($id), $currency, self::CACHE_TTL);
        }

        return $currency;
    }

    /**
     * @param $id
     * @return Currency|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function getFromRemoteProvider(string $id): ?Currency
    {
        try {
            $currency = $this->remoteProvider->get($id);
            if ($currency) {
                $this->cache->set($this->getCacheName($id), $currency, self::CACHE_TTL);
                $this->storeInDB($currency);
            }

        } catch (LockedException $e) {

            $currency = $this->findInCacheOrDB($id);

            for ($attempts = 0; !$currency; ++$attempts) {
                if ($attempts >= self::MAX_GET_REMOTE_ATTEMPTS) {
                    throw new ServiceUnavailableHttpException();
                }
                usleep(self::REMOTE_ATTEMPTS_DELAY);
            }
        }

        return $currency;
    }

    /**
     * @param string $id
     * @return string
     */
    private function getCacheName(string $id): string
    {
        return 'currency.' . $id;
    }

    private function storeInDB(Currency $currency)
    {
//        $this->_em->persist($currency);
//        $this->_em->flush();
    }
}