http://localhost/currencies/EUR

http://localhost/currencies/AED

На практике я бы рассмотрел вариант Doctrine Second Level Cache. Это закроет вопросы с ассоциациями.
Для нескольких бэкендов лучше перенести кэш на какой-нибудь не локальный key-value storage.
Можно было бы диспатчить евент в CurrencyRemoteProvider для сохранения сущности в кэше и бд при любом получении извне, но решил не плодить классы в этом конкретном случае.
К тому же CurrencyRepository тут можно считать информационным экспертом.

В случае протухания updated_at сущности в бд(курс должен быть в какой-то степени актуальным) лучше было бы конечно 
обновить его асинхронно(задача в очередь, libevent библиотеки типа ReactPhp я не рассматриваю ради снижения нагрузки на основную железку/железки),
но только если не сильно протухло, иначе синхронно.
Еще вариант по крону удалять давно не используемые. В данном конкретном случае данные должны быть актуальны, потому нет смысла хранить старые.